# COOKIEHUB INTEGRATION

## INTRODUCTION

The Cookiehub module provides easy cookiehub integration in your website.
This module implements the cookiehub integration code for the cookie consent banner and provides a field
plugin for adding the CookieHub cookie declaration to any entity type.

## REQUIREMENTS

This module has no hard dependencies.

## INSTALLATION

Install this module as any other Drupal module, see the documenation on
[Drupal.org](https://www.drupal.org/docs/extending-drupal/installing-modules).

## CONFIGURATION

### CookieHub Banner

You will find site wide configuration after installation at
`admin/config/services/cookiehub`.

### CookieHub Cookie Declaration

To add the CookieHub cookie declaration to a entity, just add the `CookieHub cookie declaration` field type.

### Opening the cookie settings dialog

Add the following weblink: `<a href="#" class="ch2-open-settings-btn">Open settings</a>`
