<?php

namespace Drupal\cookiehub\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Defines the 'cookiehub_cookie_declaration_widget' field widget.
 *
 * @FieldWidget(
 *   id = "cookiehub_cookie_declaration_widget",
 *   label = @Translation("CookieHub cookie declaration"),
 *   field_types = {"cookiehub_cookie_declaration"},
 * )
 */
class CookieDeclarationWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];
    $element['value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cookie declaration'),
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : 0,
    ];

    return $element;
  }

}
