<?php

namespace Drupal\cookiehub\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure OUM Cookiehub settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookiehub_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cookiehub.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $cookiehub_settings = $this->config('cookiehub.settings');

    $form['id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Your CookieHub code'),
      '#placeholder' => 'xxxxxxxx',
      '#default_value' => $cookiehub_settings->get('id') ? $cookiehub_settings->get('id') : '',
      '#description' => $this->t('Get your 8 digit CookieHub code from your account: <a target="_blank" href="https://dash.cookiehub.com/domain">https://dash.cookiehub.com/domain</a>'),
    ];

    $form['dev_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable development mode'),
      '#default_value' => $cookiehub_settings->get('dev_mode') ? $cookiehub_settings->get('dev_mode') : '',
    ];

    $form['automatic_cookie_blocking'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable automatic cookie blocking'),
      '#default_value' => $cookiehub_settings->get('automatic_cookie_blocking') ? $cookiehub_settings->get('automatic_cookie_blocking') : TRUE,
    ];

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the CookieHub cookie consent banner'),
      '#default_value' => $cookiehub_settings->get('enable') ? $cookiehub_settings->get('enable') : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('cookiehub.settings')
      ->set('id', $form_state->getValue('id'))
      ->set('dev_mode', $form_state->getValue('dev_mode'))
      ->set('automatic_cookie_blocking', $form_state->getValue('automatic_cookie_blocking'))
      ->set('enable', $form_state->getValue('enable'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
