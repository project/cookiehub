<?php

namespace Drupal\cookiehub\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Cookiehub cookie declaration' formatter.
 *
 * @FieldFormatter(
 *   id = "cookiehub_cookie_declaration_formatter",
 *   label = @Translation("CookieHub cookie declaration"),
 *   field_types = {
 *     "cookiehub_cookie_declaration"
 *   }
 * )
 */
class CookieDeclarationFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      if ($item->value) {
        $element[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['cookiehub-declaration'],
          ],
          '#attached' => [
            'library' => ['cookiehub/cookiehub'],
          ],
        ];
      }
    }
    return $element;
  }

}
